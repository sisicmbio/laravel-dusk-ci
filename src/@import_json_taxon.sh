#! /bin/bash
set -ex
rm -f mv_taxonomia.csv views

PDCI_SOLR_VIRTUAL_HOST=solr
PDCI_DB_PASSWORD=postgres
PDCI_DB_NAME=db_dev_cotec
PDCI_DB_USER=postgres
PDCI_DB_HOST=salve_db

#(
#echo "DROP TABLE IF EXISTS solr_views;
#CREATE TABLE IF NOT EXISTS solr_views AS
#SELECT v.*
#  FROM (VALUES ( 'taxonomia' , 'SELECT (json_trilha->''reino''->>''no_taxon'')::text as reino,
#            (json_trilha->''filo''->>''no_taxon'')::text as filo,
#            (json_trilha->''classe''->>''no_taxon'')::text as classe,
#            (json_trilha->''ordem''->>''no_taxon'')::text as ordem,
#            (json_trilha->''familia''->>''no_taxon'')::text as familia,
#            (json_trilha->''genero''->>''no_taxon'')::text as genero,
#            (json_trilha->''especie''->>''no_taxon'')::text as especie,
#            (json_trilha->''subespecie''->>''no_taxon'')::text as subespecie,
#            array_to_string(ARRAY[''texto1'', ''texto2'', ''texto3'', NULL, ''texto4''], '','', ''*'')::text as string_agg,
#            array_to_string(ARRAY[''texto1'', ''texto2'', ''texto3'', ''texto4''], '','', ''*'')::text as string_sm_null_agg,
#            ''{\"texto1\",\"texto2\",\"texto3\"}''::text[] as text_agg,
#            ''{1,2,3}''::integer[] as integer_agg,
#            sq_taxon as id,
#            sq_taxon,
#            sq_taxon_pai,
#            sq_nivel_taxonomico,
#            sq_situacao_nome,
#            no_taxon,
#            st_ativo,
#            no_autor,
#            nu_ano,
#            in_ocorre_brasil,
#            de_autor_ano,
#            no_taxon_formatado,
#            de_nivel_taxonomico,
#            nu_grau_taxonomico,
#            in_nivel_intermediario,
#            co_nivel_taxonomico
#            FROM taxonomia.vw_taxonomia
#            LIMIT 1000')
#            ,
#            ('taxonomia' , 'SELECT (json_trilha->''reino''->>''no_taxon'')::text as reino,
#            (json_trilha->''filo''->>''no_taxon'')::text as filo,
#            (json_trilha->''classe''->>''no_taxon'')::text as classe,
#            (json_trilha->''ordem''->>''no_taxon'')::text as ordem,
#            (json_trilha->''familia''->>''no_taxon'')::text as familia,
#            (json_trilha->''genero''->>''no_taxon'')::text as genero,
#            (json_trilha->''especie''->>''no_taxon'')::text as especie,
#            (json_trilha->''subespecie''->>''no_taxon'')::text as subespecie,
#            array_to_string(ARRAY[''texto1'', ''texto2'', ''texto3'', NULL, ''texto4''], '','', ''*'')::text as string_agg,
#            array_to_string(ARRAY[''texto1'', ''texto2'', ''texto3'', ''texto4''], '','', ''*'')::text as string_sm_null_agg,
#            ''{\"texto1\",\"texto2\",\"texto3\"}''::text[] as text_agg,
#            ''{1,2,3}''::integer[] as integer_agg,
#            sq_taxon as id,
#            sq_taxon,
#            sq_taxon_pai,
#            sq_nivel_taxonomico,
#            sq_situacao_nome,
#            no_taxon,
#            st_ativo,
#            no_autor,
#            nu_ano,
#            in_ocorre_brasil,
#            de_autor_ano,
#            no_taxon_formatado,
#            de_nivel_taxonomico,
#            nu_grau_taxonomico,
#            in_nivel_intermediario,
#            co_nivel_taxonomico
#            FROM taxonomia.vw_taxonomia
#            LIMIT 1000')
#            ) v(no_view, sql);";
#
#) | PGPASSWORD="${PDCI_DB_PASSWORD}" psql \
#  -v ON_ERROR_STOP=1 \
#  -t \
#  -h  "${PDCI_DB_HOST}" \
#  -p 5432 \
#  -U "${PDCI_DB_USER}" \
#  -d "${PDCI_DB_NAME}" && \
#PGPASSWORD="${PDCI_DB_PASSWORD}" psql \
#  -v ON_ERROR_STOP=1 \
#  -At \
#  -h "${PDCI_DB_HOST}" \
#  -p 5432 \
#  -U "${PDCI_DB_USER}" \
#  -d "${PDCI_DB_NAME}" \
#  -c "SELECT no_view, sql from solr_views" \
#  -o views \
#
#while read p; do
#  eval "${p}";
#done <views && \
#
##cat views
#
#exit 1;

#v_sql="SELECT (json_trilha->'reino'->>'no_taxon')::text as reino,
#            (json_trilha->'filo'->>'no_taxon')::text as filo,
#            (json_trilha->'classe'->>'no_taxon')::text as classe,
#            (json_trilha->'ordem'->>'no_taxon')::text as ordem,
#            (json_trilha->'familia'->>'no_taxon')::text as familia,
#            (json_trilha->'genero'->>'no_taxon')::text as genero,
#            (json_trilha->'especie'->>'no_taxon')::text as especie,
#            (json_trilha->'subespecie'->>'no_taxon')::text as subespecie,
#            array_to_string(ARRAY['texto1', 'texto2', 'texto3', NULL, 'texto4'], ',', '*')::text as string_agg,
#            array_to_string(ARRAY['texto1', 'texto2', 'texto3', 'texto4'], ',', '*')::text as string_sm_null_agg,
#            '{\"texto1\",\"texto2\",\"texto3\"}'::text[] as text_agg,
#            '{1,2,3}'::integer[] as integer_agg,
#            sq_taxon as id,
#            sq_taxon,
#            sq_taxon_pai,
#            sq_nivel_taxonomico,
#            sq_situacao_nome,
#            no_taxon,
#            st_ativo,
#            no_autor,
#            nu_ano,
#            in_ocorre_brasil,
#            de_autor_ano,
#            no_taxon_formatado,
#            de_nivel_taxonomico,
#            nu_grau_taxonomico,
#            in_nivel_intermediario,
#            co_nivel_taxonomico
#            FROM taxonomia.vw_taxonomia
#            ";
v_sql="select motor.sq_taxon as id
      ,motor.sq_taxon
      ,motor.reino
      ,motor.filo
      ,motor.classe
      ,motor.ordem
      ,motor.familia
      ,motor.genero
      ,motor.especie
      ,motor.subespecie
      ,motor.categoria_avaliacao
      ,motor.criterio_avaliacao
      ,motor.ano_avaliacao
      ,motor.possivelmente_extinta
      ,motor.codigo_categoria_avaliacao
      ,array_to_string( array_agg(uc.sg_uc),', ') AS sg_uc_agg
      from (
              select taxon.sq_taxon
              , json_trilha->'reino'->>'no_taxon'::text as reino
              , json_trilha->'filo'->>'no_taxon'::text as filo
              , json_trilha->'classe'->>'no_taxon'::text as classe
              , json_trilha->'ordem'->>'no_taxon'::text as ordem
              , json_trilha->'familia'->>'no_taxon'::text as familia
              , json_trilha->'genero'->>'no_taxon'::text as genero
              , json_trilha->'especie'->>'no_taxon'::text as especie
              , json_trilha->'subespecie'->>'no_taxon'::text as subespecie
              , avaliacao.de_categoria_avaliacao as categoria_avaliacao
              , avaliacao.nu_ano_avaliacao as ano_avaliacao
              , avaliacao.de_criterio_avaliacao as criterio_avaliacao
              , avaliacao.st_possivelmente_extinta as possivelmente_extinta
              , avaliacao.cd_categoria_avaliacao as codigo_categoria_avaliacao
              from taxonomia.taxon
              inner JOIN LATERAL (
                 select ha.sq_taxon
                   , categoria.ds_dados_apoio as de_categoria_avaliacao
                   , categoria.cd_dados_apoio as cd_categoria_avaliacao
                   , ha.nu_ano_avaliacao
                   , ha.de_criterio_avaliacao_iucn as de_criterio_avaliacao
                   , ha.st_possivelmente_extinta
                   from salve.taxon_historico_avaliacao ha
                  inner join salve.dados_apoio categoria on categoria.sq_dados_apoio = ha.sq_categoria_iucn
                  where ha.sq_tipo_avaliacao = 311
                    and ha.sq_taxon = taxon.sq_taxon
                  order by ha.nu_ano_avaliacao desc limit 1
              ) as avaliacao on true

      ) motor
      inner join lateral (
              select distinct ucFederal.sg_unidade_org as sg_uc, ucFederal.co_cnuc
                from salve.ficha_ocorrencia fo
              inner join corporativo.vw_unidade_org ucFederal on ucFederal.sq_pessoa = fo.sq_uc_federal
              inner join salve.ficha on ficha.sq_ficha = fo.sq_ficha
              WHERE fo.sq_contexto = 420 and
              ficha.sq_taxon = motor.sq_taxon
      ) as uc on true
      group by motor.sq_taxon
      ,motor.reino
      ,motor.filo
      ,motor.classe
      ,motor.ordem
      ,motor.familia
      ,motor.genero
      ,motor.especie
      ,motor.subespecie
      ,motor.categoria_avaliacao
      ,motor.criterio_avaliacao
      ,motor.ano_avaliacao
      ,motor.possivelmente_extinta
      ,motor.codigo_categoria_avaliacao
      order by motor.genero, motor.especie
      ";

(
    echo "SELECT '';

    SELECT 'Lendo estrutura da view';
    DROP TABLE IF EXISTS tmp_view_table;
    CREATE TABLE IF NOT EXISTS tmp_view_table AS
    ${v_sql} ;

    \COPY ( select * from tmp_view_table ) TO 'mv_taxonomia.csv' WITH (FORMAT CSV, HEADER);
  ";
) | PGPASSWORD="${PDCI_DB_PASSWORD}" psql \
  -v ON_ERROR_STOP=1 \
  -t \
  -h  "${PDCI_DB_HOST}" \
  -p 5432 \
  -U "${PDCI_DB_USER}" \
  -d "${PDCI_DB_NAME}" && \
(
    echo "SELECT 'curl -X POST -H ''Content-type:application/json'' --data-binary '''||
              json_build_object('add-field',json_build_object(  'name', column_name,
							'type', case
								 WHEN udt_name ilike 'int%' then 'text_general'
								 WHEN udt_name ilike 'bool' then 'boolean'
								 WHEN udt_name ilike 'int%' then 'text_general'
								 else 'string'
								end,
							'multiValued',CASE
									 WHEN data_type = 'ARRAY' THEN true
									 WHEN column_name ilike '%_agg' then true
									 else false
									end,
							'stored', true
						    )
						    )
       ||''' http://${PDCI_SOLR_VIRTUAL_HOST}:8983/solr/taxonomia/schema'
      FROM information_schema.columns
     WHERE --table_schema = 'lafsisbio'
       --AND
       table_name   = 'tmp_view_table'
  ";
) | PGPASSWORD="${PDCI_DB_PASSWORD}" psql \
  -v ON_ERROR_STOP=1 \
  -At \
  -h  "${PDCI_DB_HOST}" \
  -p 5432 \
  -U "${PDCI_DB_USER}" \
  -d "${PDCI_DB_NAME}" \
  -o add-field && \

curl "http://${PDCI_SOLR_VIRTUAL_HOST}:8983/solr/admin/collections?action=DELETE&name=taxonomia" || true && \
curl "http://${PDCI_SOLR_VIRTUAL_HOST}:8983/solr/admin/collections?action=CREATE&name=taxonomia&numShards=1" && \
curl "http://${PDCI_SOLR_VIRTUAL_HOST}:8983/solr/taxonomia/config" -d '{"set-user-property": {"update.autoCreateFields":"false"}}' && \
while read p; do
  eval "${p}";
done <add-field && \
sleep 5 && \
post -c taxonomia mv_taxonomia.csv
#PGPASSWORD=${PDCI_DB_PASSWORD} \
#psql -d ${PDCI_DB_NAME} \
#--echo-queries \
#-U ${PDCI_DB_USER}  \
#-h  ${PDCI_DB_HOST}  \
#-L "sintax_export.log"  \
#-c "\COPY ( ${v_sql}
#            LIMIT 100
#           ) TO 'mv_taxonomia.csv'
#     WITH (FORMAT CSV, HEADER
#       );" && \
#post -c taxonomia mv_taxonomia.csv

#echo "\COPY ( ${v_sql}
#            LIMIT 100
#           ) TO 'mv_taxonomia.csv'
#     WITH (FORMAT CSV, HEADER
#       );" > copy.sql && \

#PGPASSWORD=${PDCI_DB_PASSWORD} \
#psql -d ${PDCI_DB_NAME} \
#--echo-queries \
#-U ${PDCI_DB_USER}  \
#-h  ${PDCI_DB_HOST}  \
#-L "sintax_export.log"  << EOF
#\COPY ( select * from tmp_view_table ) TO 'mv_taxonomia.csv'
#     WITH (FORMAT CSV, HEADER
#       );
#EOF

#) | PGPASSWORD="${PDCI_DB_PASSWORD}" psql \
#  -v ON_ERROR_STOP=1 \
#  -t \
#  -h  "${PDCI_DB_HOST}" \
#  -p 5432 \
#  -U "${PDCI_DB_USER}" \
#  -d "${PDCI_DB_NAME}" && \
#(
#    echo "SELECT json_agg(json_build_object('add-field',json_build_object(  'name', column_name,
#							'type', case
#								 WHEN udt_name ilike 'int%' then 'text_general'
#								 WHEN udt_name ilike 'bool' then 'boolean'
#								 WHEN udt_name ilike 'int%' then 'text_general'
#								 else 'string'
#								end,
#							'multiValued',CASE
#									 WHEN data_type = 'ARRAY' THEN true
#									 WHEN column_name ilike '%_agg' then true
#									 else false
#									end,
#							'stored', true
#						    )
#						    )
#       )
#      FROM information_schema.columns
#     WHERE --table_schema = 'lafsisbio'
#       --AND
#       table_name   = 'tmp_view_table'
#  ";
#) | PGPASSWORD="${PDCI_DB_PASSWORD}" psql \
#  -v ON_ERROR_STOP=1 \
#  -At \
#  -h  "${PDCI_DB_HOST}" \
#  -p 5432 \
#  -U "${PDCI_DB_USER}" \
#  -d "${PDCI_DB_NAME}" \
#  -o add-field && \
#curl 'http://localhost:8983/solr/admin/collections?action=DELETE&name=taxonomia' || true && \
#curl 'http://localhost:8983/solr/admin/collections?action=CREATE&name=taxonomia&numShards=1' && \
#curl 'http://localhost:8983/solr/taxonomia/config' -d '{"set-user-property": {"update.autoCreateFields":"false"}}' && \
#
#v_add_field=$(cat add-field | sed -e "s|\[|\{|" | sed -e "s|\]|\}|")
#
#v_cmd= "curl -X POST -H 'Content-type:application/json' --data-binary '${v_add_field}' http://localhost:8983/solr/taxonomia/schema"
#eval ${v_cmd}
#echo "asdasdsadsadasdasd"
#echo "${v_cmd} " > /tmp/src/exec_add_field
#/tmp/src/exec_add_field
#exit 1;

