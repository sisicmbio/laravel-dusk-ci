#!/usr/bin/env bash

/usr/bin/php artisan dusk && \
./vendor/bin/phpunit && \
/usr/local/bin/sonar-scanner && \
exit 0
