<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

/**
 * Rotas do módulo administrativo sendo desenvolvido em VUEjs 3
 */
Route::middleware('jwt')->prefix('admin')->group(  function() {
    Route::any('/livre', 'AdminController@livre');
    Route::any('/testarAcesso', 'AdminController@testarAcesso');
    Route::any('/servicos', 'AdminController@listServices');
    Route::post('/servico/save', 'AdminController@saveService');
    Route::post('/servico/edit', 'AdminController@editService');
    Route::post('/servico/delete', 'AdminController@deleteService');
    Route::post('/servico/toggleEnabled', 'AdminController@toggleEnabledService');
    Route::post('/servico/runNow', 'AdminController@runNowService');
});


//Route::get('/{servico}', 'SolariumController@search');
//Route::get('/ping', 'SolariumController@ping');


/**
 * Utilizando SOLR com plugin Solarium
 */
Route::prefix('solr')->group(function(){
    Route::get('/teste', 'SolariumController@teste');
    Route::get('/collections', 'SolariumController@collections');
    Route::get('/fields/{collection}'  , "SolariumController@fields");
    Route::get('/{collection}/{page?}/{rows?}/'  , "SolariumController@search");


    //Route::get('/{collection}/', "SolariumController@search");
});


/**
 * Utilizando Elasticsearch com plugin
 */
Route::prefix('es')->group(function(){
    Route::get('/{collection}', "SolariumController@search");
    Route::get('/fields', "SolariumController@fields");


    //Route::get('/{collection}/', "SolariumController@search");
});


/**
 * Utilizando proxyApi para centralizar as Apis dos sistemas que necessitem
 */
Route::prefix('proxy')->group(function(){
    Route::get('/{sistema}', "SolariumController@search");
});
