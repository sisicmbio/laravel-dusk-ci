<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servico extends Model
{
    protected $table        = 'api.servico';
    protected $primaryKey   = 'sq_servico';
    protected $dateFormat   = 'yyyy-DD-mm hh:mm:ss';
    public $timestamps      = false;

    const CREATED_AT        = null;
    const UPDATED_AT        = null;

    // valores default
    protected $attributes = [
        'st_ativo' => false,
        'st_executando' => false,
        'ds_intervalo' => '1 day',
    ];

    protected $fillable = [
        'sq_servico','no_servico','ds_servico','ds_url','tx_exemplo','st_ativo','no_unidade_responsavel','no_pessoa_resposnavel','tx_contato','tx_sql','no_coluna_id','no_coluna_data','no_colecao','no_sistema','ds_intervalo',
    ];

}



