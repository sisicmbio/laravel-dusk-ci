<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;

/**
 * Classe para receber os dados de login do SICA-e, gerar o token JWT
 * e enviá-lo de volta para o cliente VUEjs na rota /login/:token
 *
 * Class AuthController
 * @package App\Http\Controllers
 */
class AuthController extends Controller {

    // receber os dados do SICA-E
    public function login( Request $request) {

        try {
            // permitir somente login vindos do SICA-e
            if (!$request->has('sqSistema')) {
                throw new \Exception("Login permitido somente a partir do SICA-e");
            }

            $userIp = $request->ip();
            if ( ! preg_match('/^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$/', $userIp)) {
                throw new \Exception("IP $userIp inválido");
            }

            // ler as informações que o SICA-e retornou
            $data = $request->all();

            // cookie da sessão php do SICA-E
            $data['cookie'] = $_COOKIE[config('jwt.sicae_cookie_name')];

            if (empty($data['cookie'])) {
                throw new \Exception('Cookie ['.config('jwt.sicae_cookie_name').'] inexistente');
            }

            //Verificar se a sigla do sistema e ada API
//            dd(config('app.app_sigla'));
            if ($data['sgSistema'] != config('app.app_sigla')) {
                throw new \Exception('Sistema inválido');
            }

            // ler os dados oo uśuário do SICA-e informando o cookie
            $opts = array(
                'http' => array(
                    'method' => "GET",
                    'header' => "Accept-language: en\r\n" .
                        "Cookie: sisicmbio=" . $data['cookie'] . "\r\n"
                )
            );

            $context = stream_context_create($opts);
            $dadosSicae = json_decode(file_get_contents(config('jwt.url_sicae').'session.php', false, $context));

            if (empty($dadosSicae->noUsuario)) {
                throw new \Exception('Login inválido');;
            }

            //dd($dadosSicae);
            // gerar o jwt token


            //Header Token
            $header = [
                'typ' => 'JWT',
                'alg' => 'HS256'
            ];

            //Payload - Content
            $payload = [
                'nome'   => $dadosSicae->noUsuario,
                'cpf'    => $dadosSicae->nuCpf,
                'perfil' => $dadosSicae->noPerfil,
                'unidade'=> $dadosSicae->noUnidadeOrg,
                'sistema'=> $dadosSicae->noSistema,
                'cookie' => $data['cookie']
            ];

            //JSON
            $header = json_encode($header);
            $payload = json_encode($payload);

            //Base 64
            $header = $this->base64urlEncode($header);
            $payload = $this->base64urlEncode($payload);

            //Sign
            $key = config('jwt.key') . preg_replace('/[^0-9]/', '', $userIp );

            $sign = hash_hmac('sha256', $header . "." . $payload, $key, true);
            $sign = $this->base64urlEncode($sign);

            //Token
            $token = $header . '.' . $payload . '.' . $sign;
            $dadosSicae->token = $token;

            return redirect( config('jwt.url_frontend')."login/" . $token);
        } catch (\Exception $e) {
            return redirect('/')->withFail($e->getMessage() );;
        }
    }

    /**
     * método utilizado para criação do token JWT
     * @param $data
     * @return false|string
     */
    protected function base64urlEncode($data){
        // First of all you should encode $data to Base64 string
        $b64 = base64_encode($data);

        // Make sure you get a valid result, otherwise, return FALSE, as the base64_encode() function do
        if ($b64 === false) {
            return false;
        }

        // Convert Base64 to Base64URL by replacing “+” with “-” and “/” with “_”
        $url = strtr($b64, '+/', '-_');

        // Remove padding character from the end of line and return the Base64URL result
        return rtrim($url, '=');
    }
}
