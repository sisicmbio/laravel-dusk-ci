<?php
namespace App\Http\Controllers;

//app('sentry')->captureException( new Exception('Ok teste bem sucedido!') );

use Illuminate\Http\Request;
use Mockery\Exception;
use Solarium\Client;

class SolariumController extends Controller
{
    protected $client;
    const MAX_ROWS = 500;
    const PAGE=1;

    public function __construct( Client $client )
    {
       $this->client = $client;
    }

    /**
     * Calcular o tempo decorrido entre duas datas e retornar o resultado formatado
     * @param null $startTime
     * @return string
     */
    protected function _getFormatedEllapsedTime( $startTime = null ) {

        if( empty( $startTime ) ) {
            return '';
        }
        $endTime            = now();
        $elapsedTime        = $endTime->diff( $startTime );
        $minuteDayMonth     = [];
        if( $elapsedTime->d  > 0 ) {
            array_push($minuteDayMonth,$elapsedTime->format('%d dia(s)') );
        }
        if( $elapsedTime->h  > 0 ) {
            array_push($minuteDayMonth,$elapsedTime->format('%H dia(s)') );
        }
        if( $elapsedTime->i  > 0 ) {
            array_push($minuteDayMonth,$elapsedTime->format('%i minuto(s)') );
        }
        $seconds = $elapsedTime->format('%s segundos(s)');
        if( ! empty( $minuteDayMonth ) )
        {
            $seconds = implode(', ', $minuteDayMonth ). ' e '.$seconds;
        }
        return $seconds;
    }

    /**
     * Lista as colecções cadastradas para validação interna das requisições
     *
     */
    protected function _getCollections() {
        $url    = "http://".env('SOLR_HOST', 'solr').":".env('SOLR_PORT', '8983')."/solr/admin/collections?action=LIST&wt=json";
        $data   = json_decode( file_get_contents($url) );
        return $data->collections;
    }

    /**
     * Método para validar se a coleção informada existe no solr
     * Se não retorar a exceção é porque a coleção existe
     * @param $collection
     */
    protected function _validateCollection($collection=null) {
        try {

            // coleção não pode ser vazia
            if( empty( trim( $collection ) ) ) {
                throw new \Exception('Necessário informar o nome da coleção');
            }

            $collections = $this->_getCollections();
            //dd( $collections );

            if( ! is_array( $collections ) || array_search( $collection, $collections ) === false ) {
                throw new \Exception('Coleção '.$collection.' não existe.');
            }

        } catch ( \Exception $e ) {

            die( json_encode( ['status'=>'500','error'=>$e->getMessage()] ) );

        }
    }

    /**
     * método para definir a coleção que será utilizada para pesquisa
     * @param null $collection
     */
    protected function _setCollection( $collection = null ){
        $this->_validateCollection($collection);
        $options = [
            'endpoint' => [
                'default' => [
                    'host' => env('SOLR_HOST', 'solr'),
                    'port' => env('SOLR_PORT', '8983'),
                    'core' => $collection
                ]
            ]
        ];
        $this->client->setOptions( $options,false);
    }


    /**
     * Testar se o SOLR está ok listando as coleções existentes
     * @return json
     *
     * @OA\Get (
     *      path="/api/solr/teste",
     *      operationId="teste",
     *      tags={"Testes"},
     *
     *      summary="Testar servidor SOLR",
     *      description="Retorna a lista de esquemas",
     * @OA\Response(
     *    response=200,
     *     description="Operação realizada com sucesso",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *     )
     * ),
     * @OA\Response(
     *     response=401,
     *     description="Não autorizado",
     * ),
     * @OA\Response(
     *     response=403,
     *     description="Proibido"
     * ),
     * @OA\Response(
     *      response=400,
     *      description="Requisição inválida"
     *   ),
     * @OA\Response(
     *      response=404,
     *      description="Endereço não encontrado"
     *   ),
     *  )
     */
    public function teste()
    {
        $result = [];
        $collections = $this->_getCollections();
        if( count( $collections ) > 0 ) {
            $result = ['status' => '200', 'msg'=>'SOLR OK!','data'=>$collections];
        } else {
            $result = ['status' => '200', 'msg'=>'SOLR OK!<br>Nenhuma coleção foi cadastrada.','data'=>[]];
        }

        return response()->json( $result );

        // $this->_validateCollection('');
        // $this->_validateCollection('especie_uc');
        // die( json_encode( ['status' => '200', 'msg'=>'Coleção existe!!!'] ) );
        //dd('Ping ok');
        //return ;

    }
    /**
     * Listar as coleções existentes
     * @return json
     *
     * @OA\Get (
     *      path="/api/solr/collections",
     *      operationId="collections",
     *      tags={"Serviços"},
     *
     *      summary="Listar as coleções",
     *      description="Retorna a lista de coleções cadastradas no SOLR",
     *
     * @OA\Response(
     *     response=200,
     *     description="Operação realizada com sucesso",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *          @OA\Schema(
     *              @OA\Property(property="nome", type="string", description="nome da coleção")
     *          ),
     *          @OA\Examples(summary="",value={"nome":"especie_uc","campo2":"valor2"}),
     *     ),
     * ),
     * @OA\Response(
     *     response=401,
     *     description="Não autorizado",
     * ),
     *
     * @OA\Response(
     *     response=403,
     *     description="Proibido"
     * ),
     * @OA\Response(
     *      response=400,
     *      description="Requisição inválida"
     *   ),
     * @OA\Response(
     *      response=404,
     *      description="Endereço não encontrado"
     *   ),
     *  )
     */
    public function collections()
    {
        $result = [];
        $collections = $this->_getCollections();
        if( count( $collections ) > 0 ) {
            $result = ['status' => '200', 'msg' => '','data' => $collections ];
        } else {
            $result = ['status' => '200', 'msg'=>'<br>Nenhuma coleção cadastrada.','data'=>[] ];
        }

        return response()->json( $result );

        // $this->_validateCollection('');
        // $this->_validateCollection('especie_uc');
        // die( json_encode( ['status' => '200', 'msg'=>'Coleção existe!!!'] ) );
        //dd('Ping ok');
        //return ;
    }


    /**
     * Lista as colunas disponíveis na coleção
     * @example <collection>/fields
     * @param $collection - nome da coleção cadastrada
     * @return json
     *
     * @OA\Get (
     *      path="/fields/{collection}",
     *      operationId="fields",
     *      tags={"Serviços"},
     *
     *      summary="Lista de campos",
     *      description="Retorna a lista campos de uma coleção",
     * @OA\Parameter(
     *      name="collection",
     *      in="path",
     *      example="especie_uc",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     *
     * @OA\Response(
     *    response=200,
     *     description="Operação realizada com sucesso",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *              @OA\Schema (
     *                  @OA\Property (
     *                      property="nome",
     *                      type="string",
     *                      example="especie_uc"
     *                  )
     *              )
     *          )
     *    )
     * )
     *
     *)
     */
    public function fields( $collection ) {
        $this->_validateCollection($collection);

        //dd('http://'.env('SOLR_HOST', 'solr').':'.env('SOLR_PORT', '8983').'/solr/' . $collection . '/select?q=*:*&wt=csv&rows=0');


        try {
            $fields = file_get_contents('http://'.env('SOLR_HOST', 'solr').':'.env('SOLR_PORT', '8983').'/solr/' . $collection . '/select?q=*:*&wt=csv&rows=0');
        } catch( \Exception $e) {
            $message = $e->getMessage();
            if( preg_match('/404 Not Found/',$message) ) {
                return ['erro'=>'Coleção: '.$collection.' não encontrada.'];
            }
            return ['erro'=>$message];
        }
        $result = ['status'=>'200', 'collection'=>$collection, 'data' => $fields ];
        return response()->json( $result );
    }


    /**
     * método para realizar as consultas no SOLR
     * @param Request $request
     * @param $collection
     * @param int $page
     * @param int $rows
     * @return array
     * @example http://dev.api.icmbio.gov.br/api/solr/especie_uc/1/100?reino=Animalia
     *
     * @OA\Get (
     *      path="/api/solr/{collection}/{page}/{rows}",
     *      operationId="search",
     *      tags={"Serviços"},
     *
     *      summary="Pesquisar espécies",
     *      description="Pesquisar as espécies que ocorrem em UCs federais",
     *
     * @OA\Parameter(
     *      name="collection",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * @OA\Parameter(
     *      name="page",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *           type="integer"
     *      )
     * ),
     * @OA\Parameter(
     *      name="rows",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *           type="integer"
     *      )
     * ),
     * @OA\Parameter(
     *      name="reino",
     *      in="query",
     *      required=false,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     *
     * @OA\Response(
     *    response=200,
     *     description="Operação realizada com sucesso",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *     )
     * )
     *)
     */

    public function search( Request $request, $collection,$page=self::PAGE,$rows=self::MAX_ROWS)
    {
        // validar e definir a coleção informada para pesquisa
        $this->_setCollection($collection);

        // calcular e retornar a duração no resultado da pesquisa
        $startTime = now();

        // ler os parâmetros informados pelo usuário na url para filtragem dos dados
        $params = $request->all();

        // quantidade de registros que serão retornados

        if ($rows > self::MAX_ROWS) {
            die( json_encode( ['erro'=>'O máximo de registro permitido para retorno é de '.self::MAX_ROWS.'.' ] ) );
        }


//        if( array_key_exists('limit' ,$params) ) {
//            $rows  = $params['limit'];
//            unset($params['limit']);
//        }

        // número da página que será retornada
//        $page = 1;
//        if( array_key_exists('page' ,$params) ) {
//            $page  = $params['page'];
//            unset($params['page']);
//        }

        // calcular o registro inicial de acordo com a página solicitada
        $start = ( $page - 1 ) * $rows;

        // criar o objeto query para passagem dos parâmetros de pesquisa
        $query = $this->client->createQuery( $this->client::QUERY_SELECT, array('responsewriter' => 'json') );

        $query->setQuery('*:*');
        foreach( $params as $k => $v) {
            $str = $k.':"'.$v.'"';
            $query->setQuery( $str );
        }
        $query->setStart($start);
        $query->setRows($rows);
        try {
            $res = $this->client->execute($query);
        } catch( \Exception $e ) {
            $body = json_decode( $e->getBody());
            $errorMessage = $body->error->msg;
            $errorMessage = preg_replace(['/undefined field/'],['campos informado não existe na coleção: '],$errorMessage);
            $result = ['error'=>$errorMessage];
            return $result;
        }
        $body =  (Array) json_decode( $res->getResponse()->getBody() );
        $body['response']->page = $page;
        $body['response']->searchDate = date('d/m/Y H:i:s');
        $body['response']->elapsedTime = $this->_getFormatedEllapsedTime($startTime);
        return (Array) $body['response'];
    }

}
