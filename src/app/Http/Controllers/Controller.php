<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="Documentação da API - ICMBio",
     *      description="Listagem dos serviços disponíveis",
     *      @OA\Contact(
     *          email="admin@admin.com"
     *      )
     * )
     *
     * @OA\Server(
     *         description="Servidor API ICMBio",
     *         url="http://localhost:8000/api/solr/"
     * )
     *
     * @OA\Tag(
     *     name="Serviços",
     *     description="Relação dos serviços"
     * )
     *
     */
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
