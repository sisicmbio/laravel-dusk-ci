<?php
namespace App\Http\Controllers;

use App\Servico;
use Illuminate\Http\Request;

/**
 * Class AdminController
 * Controlador utilizado pelo frontend desenvolvido em VueJs
 * @package App\Http\Controllers
 */

class AdminController extends Controller {

    /**
     * método para testar. Não precisa estar logado.
     */
    public function livre () {
        //$lista = Servico::all();
        //print_r( $lista[0] );

        $results = \DB::select('select * from api.servico where sq_servico > ?', [0]);

        return response()->json( ["status"=>200,'msg'=>'ok, acesso livre.'
            ,'data'=> $results] );
    }

    /**
     * Método para testar se a autenciação funcionou.
     * @return \Illuminate\Http\JsonResponse
     */
    public function testarAcesso () {
        return response()->json( ["status"=>200,'msg'=>'Acesso permitido, você está autenticado.'] );
    }

    /**
     * Método para listar todos os serviços cadastrados
     */
    public function listServices() {

        $res = \DB::select("select sq_servico, no_servico, st_ativo, to_char(dt_atualizacao,'dd/mm/yyyy HH:mm:ss') as dt_atualizacao, ds_intervalo
                            from api.servico order by no_servico");
        return response()->json(
             ["status"=>200,'msg'=>'','data'=> $res]
        );

    }

    /**
     * Método para excluir  um serviço
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteService(Request $request) {
        $result = ["status" => 200, 'msg' => '', 'data' => [], 'errors' => []];
        try {
            $sq_servico = $request->input('sq_servico');
            if ( empty( $sq_servico ) ) {
                throw new \Exception('Id do serviço inexistente.');
            }
            $servico = Servico::find( $sq_servico);
            if( ! empty( $servico ) ) {
                $servico->delete();
                $result['msg']='Serviço excluído com SUCESSO.';
            } else {
                array_push($result['errors'],'Id não encontrado.');
            }
        } catch (\Exception $e) {
            array_push($result['errors'], $e->getMessage());
        }
        return response()->json($result);
    }

    /**
     * Método para ativar/desativar um serviço
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function toggleEnabledService( Request $request ) {
        $result = ["status" => 200, 'msg' => '', 'data' => [], 'errors' => []];
        try {
            $sq_servico = $request->input('sq_servico');
            if ( empty( $sq_servico ) ) {
                throw new \Exception('Id do serviço inexistente.');
            }
            $servico = Servico::find( $sq_servico);
            if( ! empty( $servico ) ) {
                $servico->st_ativo = ! $servico->st_ativo;
                $servico->save();
                $result['msg']='Alteração realizada com SUCESSO.';
            } else {
                array_push($result['errors'],'Id não encontrado.');
            }
        } catch (\Exception $e) {
            array_push($result['errors'], $e->getMessage());
        }
        return response()->json($result);
    }

    /**
     * Método para colocar o serviço em execução imedidata
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function runNowService( Request $request ) {
        $result = ["status" => 200, 'msg' => '', 'data' => [], 'errors' => []];
        try {
            $sq_servico = $request->input('sq_servico');
            if ( empty( $sq_servico ) ) {
                throw new \Exception('Id do serviço inexistente.');
            }
            $servico = Servico::find( $sq_servico);
            if( ! empty( $servico ) ) {
                if( $servico->st_executando ) {
                    throw new \Exception('Serviço já está em execução. Aguarde o término para solicitar uma nova execução.');
                }
                $servico->dt_atualizacao=null;
                $servico->st_ativo=true;
                $servico->save();
                $result['msg']='Alteração realizada com SUCESSO.';
                $result['data']['dt_atualizacao'] = date('d/m/Y H:i:s');
            } else {
                array_push($result['errors'],'Id não encontrado.');
            }
        } catch (\Exception $e) {
            array_push($result['errors'], $e->getMessage());
        }
        return response()->json($result);
    }

    /**
     * Método para editar um serviço
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function editService(Request $request) {
        $result = ["status" => 200, 'msg' => '', 'data' => [], 'errors' => []];
        try {
            $sq_servico = $request->input('sq_servico');
            if ( empty( $sq_servico ) ) {
                throw new \Exception('Id do serviço inexistente.');
            }
            $servico = Servico::find( $sq_servico);
            if( ! empty( $servico ) ) {
                $result['data'] = $servico;
            } else {
                array_push($result['errors'],'Id não encontrado.');
            }
        } catch (\Exception $e) {
            array_push($result['errors'], $e->getMessage());
        }
        return response()->json($result);
    }

    /**
     * Método para gravar o Serviço no banco de dados
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveService(Request $request ) {

        $message = 'Gravação realizadas com SUCESSO!';
        $errors = [];

        // validar campos
        $data = $request->input();
        try {
            if (empty($data['no_servico'])) {
                array_push($errors, 'Informe o nome do serviço.');
            }
            if (empty($data['ds_servico'])) {
                array_push($errors, 'Informe a descrição do serviço.');
            }

            if (empty($data['no_coluna_id'])) {
                array_push($errors, 'Informe o nome da coluna ID.');
            }
            if (empty($data['no_coluna_data'])) {
                array_push($errors, 'Informe o nome da coluna DATA.');
            }
            if (empty($data['no_colecao'])) {
                array_push($errors, 'Informe o nome da coleção.');
            }
            if (empty($data['no_sistema'])) {
                array_push($errors, 'Informe o nome do sistema.');
            }
            if (empty($data['tx_sql'])) {
                array_push($errors, 'Informe o SQL do serviço.');
            }
            if ( ! empty($errors) ) {
                $message = '';
            } else {

                $servico = null;

                if (!empty($data['sq_servico'])) {
                    $servico = Servico::find($data['sq_servico'] );
                    if( empty( $servico ) ) {
                        throw new \Exception('Id do serviço inválido');
                    }
                } else {
                    $servico = new Servico();
                }

                // campo obrigatórios
                $servico->no_servico = $data['no_servico'];
                $servico->ds_servico = $data['ds_servico'];
                $servico->no_coluna_id = $data['no_coluna_id'];
                $servico->no_coluna_data = $data['no_coluna_data'];
                $servico->no_colecao = $data['no_colecao'];
                $servico->no_sistema = $data['no_sistema'];
                $servico->ds_intervalo = isset($data['ds_intervalo']) ? $data['ds_intervalo'] : '1 day';
                $servico->tx_sql = $data['tx_sql'];
                $servico->st_ativo = ($data['st_ativo'] == true ? true : false);
                // campos não obrigatórios
                $servico->ds_url = isset($data['ds_url']) ? $data['ds_url'] : null;
                $servico->tx_exemplo = isset($data['tx_exemplo']) ? $data['tx_exemplo'] : null;
                $servico->no_unidade_responsavel = isset($data['no_unidade_responsavel']) ? isset($data['no_unidade_responsavel']) : null;
                $servico->no_pessoa_resposnavel = isset($data['no_pessoa_resposnavel']) ? $data['no_pessoa_resposnavel'] : null;
                $servico->tx_contato = isset($data['tx_contato']) ? $data['tx_contato'] : null;
                $servico->save();
                $data['sq_servico'] = $servico->sq_servico;
            }

        } catch (\Exception $e) {
            array_push($errors, $e->getMessage());
        }
        return response()->json(
            ["status" => 200, 'msg' => $message
                , 'data' => $data
                , 'errors' => $errors]
        );
    }
}
