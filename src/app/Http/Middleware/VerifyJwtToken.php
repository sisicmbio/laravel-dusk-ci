<?php
namespace App\Http\Middleware;

use Closure;

class VerifyJwtToken {
    /**
     * Ações que não precisam de estar logado
     * @var string[]
     */
    /*protected $except = [
        'api/admin/login',
    ];*/

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $path = $request->path();

        // exemplos de como remover seguranca de uma rota especifica
        if( $path == 'api/admin/livre' ) {
            return $next($request);
        }

        // tudo liberado por enquanto
        return $next($request);

        // validar Token e a Sessão SICA-e
        if( ! $this->checkAuth() ) {
            return response(['status'=>401,'msg'=>'Não autenticado, efetue login novamente'],401);
        }
        return $next($request);
    }

    /**
     * Método para validar se existe o token no cabeçalho da requisição, se ele é válido
     * e se a sessão ainda está válida no SICA-e
     * @return bool
     */
    private function checkAuth(){
        //$token =  $request->header('Authorization' );
        $http_header = apache_request_headers();
        if (isset($http_header['Authorization']) && $http_header['Authorization'] != null) {
            $bearer = explode(' ', $http_header['Authorization']);
            //$bearer[0] = 'bearer';
            //$bearer[1] = 'token jwt';

            $token      = explode('.', $bearer[1]);
            $header     = $token[0];
            $payload    = $token[1];
            $sign       = $token[2];

            //$userIp = $request->ip();
            $userIp = $ip = $_SERVER['REMOTE_ADDR'];
            if ( ! preg_match('/^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$/', $userIp)) {
                return false;
                //throw new \Exception("IP $userIp inválido");
            }

            // cookie da sessão php do SICA-E
            $data       = json_decode( base64_decode($payload) );
            $cookie     = $data->cookie;

            $key = config('jwt.key') . preg_replace('/[^0-9]/', '', $userIp );

            //Conferir Assinatura
            $valid = hash_hmac('sha256', $header . "." . $payload, $key, true);

            $valid = $this->base64urlEncode( $valid );

            if ($sign === $valid && ! empty( $cookie ) ) {

                // verificar se a sessão do sica-e ainda está valendo
                // ler os dados oo uśuário do SICA-e informando o cookie
                $opts = array(
                    'http' => array(
                        'method' => "GET",
                        'header' => "Accept-language: en\r\n" .
                            "Cookie: ".config('jwt.sicae_cookie_name')."=" . $cookie . "\r\n"
                    )
                );

                $context = stream_context_create($opts);
                $dadosSicae = json_decode(file_get_contents(config('jwt.url_sicae').'session.php', false, $context));
                if ( empty( $dadosSicae->noUsuario ) ) {
                    return false;
                }
                //return $dadosSicae->noUsuario;
                return true;
            }
        }
        return false;
    }

    /**
     * método utilizado para criação do token JWT
     * @param $data
     * @return false|string
     */
    protected function base64urlEncode($data){
        // First of all you should encode $data to Base64 string
        $b64 = base64_encode($data);

        // Make sure you get a valid result, otherwise, return FALSE, as the base64_encode() function do
        if ($b64 === false) {
            return false;
        }

        // Convert Base64 to Base64URL by replacing “+” with “-” and “/” with “_”
        $url = strtr($b64, '+/', '-_');

        // Remove padding character from the end of line and return the Base64URL result
        return rtrim($url, '=');
    }
}
