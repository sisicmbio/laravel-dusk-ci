<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
//use Solarium\Core\Client\Client;
use Solarium\Client;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Solarium\Core\Client\Adapter\Curl;
use Solarium\Core\Client\Adapter\Http;

class SolariumServiceProvider extends ServiceProvider
{
    // This will make you application faster if you don't always need the Client.
    // If your application needs it with every request remove $defer=true from the service provider.
    protected $defer = true;

    /**
     * Register any application services.
     *
     * @return  void
     */
    public function register()
    {
        $this->app->bind(Client::class, function ($app) {
            //return new Client($app['config']['solarium']);
            $adapter = new Curl();
            //$adapter = new Http();
            $eventDispatcher = new EventDispatcher();
            return  new Client($adapter, $eventDispatcher, $app['config']['solarium'] );
        });
    }

    public function provides()
    {
        return [Client::class];
    }

}
