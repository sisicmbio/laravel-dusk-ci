<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;

class AuthTest extends DuskTestCase
{

    public function test_login()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('http://sicae.api.icmbio.gov.br/usuario/logout')
                ->waitForText('Usuário');

            $browser = $this->login($browser, '69974349168', 'caio4378');

            $browser->waitForText('Bem-vindo aos Sistemas do ICMBio');

//            $browser = $this->findElementSistema($browser,'API');
            $browser->script("document.querySelector('div[data-original-title=API] button').click();");
            $browser->waitForText('API ADM')
                ->assertSee('Bem vindo');

//            $browser->assertSee('Bem vindo');

        });
    }

    public function findElementSistema(Browser $browser,$sigla){

//       $browser->driver->findElements(WebDriverBy::xpath('//div[@data-original-title="API"] button'))->click()
//            ->waitForText('API ADM');
//       ;
        $browser->script("document.querySelector('div[data-original-title=API] button').click();")
            ->waitForText('API ADM');

//        $browser->element('div[data-original-title='.$sigla.'] button')
//            ->click()
//            ->waitForText('API ADM');
        return $browser;
    }

    public function login(Browser $browser, $cpf, $password)
    {
        $browser->visit('http://sicae.api.icmbio.gov.br')
            ->type('nuLogin', $cpf)
            ->type('txSenha', $password)
            ->press('Login');

        return $browser;
    }

//    public function auth(Browser $browser, string $unidade, string $profile)
//    {
//        $browser->visit('http://test.soala.sisicmbio.icmbio.gov.br/');
//            $browser->waitForText('Unidade Organizacional / Perfil');
//            $browser->select('feijoadaUnit', $unidade);
//            $browser->pause(1000);
//            $browser->select('feijoadaProfile', $profile);
//            $browser->pause(1000);
//            $browser->element('#btn-access')->click();
//
//        return $browser;
//    }

    public function test_invalid_login()
    {
        $this->browse(function (Browser $browser) {
            $browser = $this->login($browser, '11111111112', 'qaz123');
            $browser->waitForText('Por favor, informe um CPF válido')->assertSee('Por favor, informe um CPF válido');
        });
    }
}
