<?php

return [
    'key' => env('JWT_KEY', 'E1bfb6D1ab4943f4Bbe94e46871b9443'),

    // informar a url com a barra (/) no final
    'url_sicae' => env('URL_SICAE', 'http://sicae.api.icmbio.gov.br/'),

    // informar a url com a barra (/) no final
    'url_frontend' => env('URL_FRONTEND', 'http://api.icmbio.gov.br/'),

    // nome do cookie utilizado pelo SICA-e
    'sicae_cookie_name' => env('SICAE_COOKIE_NAME', 'sisicmbio'),

];
