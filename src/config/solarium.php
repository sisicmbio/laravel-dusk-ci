<?php
/**
 * @site: https://petericebear.github.io/laravel-php-solarium-integration-20160725/
 * This configuration assumes that the SOLR is installed on the local machine,
 * uses the default port of 8983, the default solr install path and with a default
 * collection named collection1.
 * You can override every configuration setting within the .env file in your projectfolder.
 */

return [
    'endpoint' => [
        'default' => [
            'host' => env('SOLR_HOST', 'solr'),
            'port' => env('SOLR_PORT', '8983'),
            //'path' => env('SOLR_PATH', ''),
            //'core' => env('SOLR_CORE', 'taxonomia')
        ]
    ]
];
